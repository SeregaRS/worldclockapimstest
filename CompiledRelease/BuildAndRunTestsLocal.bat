:: Input Files
:: - Windows Machine with .Net Framework installed
:: - dll with tests
:: - nuget.exe app
:: - packages.config file - Libs for Output 
:: 		- Microsoft.VisualStudio.QualityTools.UnitTestFramework.dll
:: 		- Newtonsoft.Json.dll
:: 		- RestSharp.dll
:: - packages.config file - Additional tools
:: 		- vstest.console.exe - Runner for Ms unit tests
:: 		- ReportUnit.exe - report converter (trx to html)


@ECHO OFF
@ECHO %username%
@ECHO %computername%

:: Restore Nuget Packages
@SET NUGET_EXE_LOCATION=".nuget\nuget.exe"
@ECHO %NUGET_EXE_LOCATION%
@SET NUGET_PACKAGESCONFIG_LOCATION="packages.config"
@ECHO %NUGET_PACKAGESCONFIG_LOCATION%
call %NUGET_EXE_LOCATION% install %NUGET_PACKAGESCONFIG_LOCATION%
::pause

:: Copy Libs to Output Directory
copy "Microsoft.VisualStudio.QualityTools.UnitTestFramework.Updated.15.0.26228\lib\Microsoft.VisualStudio.QualityTools.UnitTestFramework.dll" "Microsoft.VisualStudio.QualityTools.UnitTestFramework.dll"
copy "Newtonsoft.Json.11.0.2\lib\net45\Newtonsoft.Json.dll" "Newtonsoft.Json.dll"
copy "RestSharp.106.3.1\lib\net452\RestSharp.dll" "RestSharp.dll"

:: Remove Nuget Package directories
:: Clean Up Acton (Might be added in the separate bat file)
RMDIR /S /Q "Microsoft.VisualStudio.QualityTools.UnitTestFramework.Updated.15.0.26228"
RMDIR /S /Q "Newtonsoft.Json.11.0.2"
RMDIR /S /Q "RestSharp.106.3.1"

:: Call Test Run
	:: VsTestConsole Location
@SET VSTEST_LOCATION="Microsoft.TestPlatform.15.8.0\tools\net451\Common7\IDE\Extensions\TestPlatform\vstest.console.exe"
@ECHO %VSTEST_LOCATION%

	:: dll with tests Location
@SET TESTDLL_LOCATION="WorldClockApiTests.dll"
@ECHO %TESTDLL_LOCATION% 
call %VSTEST_LOCATION% %TESTDLL_LOCATION% /Logger:trx

:: Call Report Creation (trx to html)
@SET REPORT_LOCATION="TestResults"
@ECHO %REPORT_LOCATION% 
@SET REPORTUNIT_EXE_PATH="ReportUnit.1.2.1\tools\ReportUnit.exe"
@ECHO %REPORTUNIT_EXE_PATH%
call %REPORTUNIT_EXE_PATH% %REPORT_LOCATION% TestRunHtmlReports

:: Clean Up Acton (Might be added in the separate bat file)
RMDIR /S /Q %REPORT_LOCATION%
RMDIR /S /Q "ReportUnit.1.2.1"
RMDIR /S /Q "Microsoft.TestPlatform.15.8.0"

pause