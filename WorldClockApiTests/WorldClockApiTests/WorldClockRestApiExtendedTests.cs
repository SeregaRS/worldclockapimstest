﻿namespace WorldClockApiTests
{
    using System.Collections.Generic;

    using Microsoft.VisualStudio.TestTools.UnitTesting;

    using Newtonsoft.Json;

    using RestSharp;


    /// <summary>
    /// Class contains tests for http://worldclockapi.com/api
    /// $id
    /// currentDateTime
    /// utcOffset
    /// isDayLightSavingsTime
    /// dayOfTheWeek
    /// timeZoneName
    /// currentFileTime
    /// ordinalDate
    /// serviceResponse
    /// </summary>
    [TestClass]
    public class WorldClockRestApiExtendedTests : WorldClockRestApiBaseTest
    {
        /// <summary>
        /// Eastern Standard Time Response Data Test
        /// </summary>
        [TestMethod]
        public void EasternStandardTimeContentTest()
        {
            var easternStandardTimeRequest = new RestRequest("json/est/now", Method.GET);
            var response = this.WorldClockApiClient.Execute(easternStandardTimeRequest);

            Assert.IsFalse(string.IsNullOrWhiteSpace(response.Content));
            var responceDictionaty = JsonConvert.DeserializeObject<Dictionary<string, string>>(response.Content);

            // Assertions.  
            Assert.IsTrue(responceDictionaty["dayOfTheWeek"].Equals(this.CurrentDayOfWeekFromSystem));
            Assert.IsFalse(string.IsNullOrWhiteSpace(responceDictionaty["currentFileTime"]));
            Assert.IsNull(responceDictionaty["serviceResponse"]);
        }

        /// <summary>
        /// Coordinated Universal Time Response Data Test
        /// </summary>
        [TestMethod]
        public void CoordinatedUniversalTimeContentTest()
        {
            var coordinatedUniversalTimeRequest = new RestRequest("json/utc/now", Method.GET);
            var response = this.WorldClockApiClient.Execute(coordinatedUniversalTimeRequest);

            Assert.IsFalse(string.IsNullOrWhiteSpace(response.Content));
            var responceDictionaty = JsonConvert.DeserializeObject<Dictionary<string, string>>(response.Content);

            // Assertions. 
            Assert.IsTrue(responceDictionaty["dayOfTheWeek"].Equals(this.CurrentDayOfWeekFromSystem));
            Assert.IsFalse(string.IsNullOrWhiteSpace(responceDictionaty["currentFileTime"]));
            Assert.IsNull(responceDictionaty["serviceResponse"]);
        }

        /// <summary>
        /// Central European Standard Time Response Data Test
        /// Call back function
        /// </summary>
        [TestMethod]
        public void CentralEuropeanStandardTimeContentTest()
        {
            var centralEuropeanStandardTimeRequest = new RestRequest("jsonp/cet/now?callback=mycallback", Method.GET);
            var response = this.WorldClockApiClient.Execute(centralEuropeanStandardTimeRequest);
            var responceContent = response.Content;

            Assert.IsFalse(string.IsNullOrWhiteSpace(responceContent));
            Assert.IsTrue(responceContent.Contains("mycallback"));

            var responceDictionaty =
                JsonConvert.DeserializeObject<Dictionary<string, string>>(
                    this.ConvertCallBackResponseToJson(responceContent));

            // Assertions. 
            Assert.IsTrue(responceDictionaty["dayOfTheWeek"].Equals(this.CurrentDayOfWeekFromSystem));
            Assert.IsFalse(string.IsNullOrWhiteSpace(responceDictionaty["currentFileTime"]));
            Assert.IsNull(responceDictionaty["serviceResponse"]);
        }

        /// <summary>
        /// Convert CallBack Response To Json
        /// </summary>
        /// <param name="responceCallBackFunction"></param>
        /// <returns></returns>
        private string ConvertCallBackResponseToJson(string responceCallBackFunction)
        {
            return responceCallBackFunction.Replace("mycallback(", string.Empty).Replace(@"});", @"}");
        }
    }
}
